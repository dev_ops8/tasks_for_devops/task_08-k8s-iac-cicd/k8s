# task_08-k8s-cicd-iac-web-app
This task simulates the real project of dockerized app

## Description of the task
We have small team includes devops group and developer's group. We must create CI/CD pipeline that can do ordinary things such as testing the developer's code, building docker images, push it to our registry and etc. But we must create the CD part, that will deploy our dockerized app to real "dev" environment on cloud provider, trigger by any commit in "<u>dev</u>" branch in <u>app repository</u> via developer's group.
So, our CI/CD pipeline can linting dev code, build docker images, push to registry and deploy it to "dev" environment on cloud provider via any commit in "dev" branch in <u>app repository</u> via developer's group. Our developer's are professional, so they always add tag's when commit to "dev" branch. When our CI/CD pipeline will deploy to cloud provider, we must reuse this tag, as a tag for our deployed apps on cloud provider environment, it gives us possibility to follow for our versioning of app. 
Also, if we must update/change K8s ifrastructure via devops team, we must be able to find the "release" version of infrastructure.
If any wrong will happen and our K8s infra down, we must rollback to previous version. 
Also we must save log's from our app to any persistent volume. It help us to analyze the behaviour of app.
Files for app take from task_04

## Check-points:
- Separate on different repository via purpose of code: app - for app code, IaC - for infra code, K8s - for K8s code
- Create CI/CD pipeline that will linting app code, build images and push to registry by any commint in branch "<u>dev</u>" in app repository
- Create CI/CD pipeline that can deploy/update version of app trigger by commits in <u>app repository</u>
- Create CI/CD pipeline that can deploy/update version of K8s infra trigger by commits in <u>K8s repository</u>  
- Create CI/CD pipeline that can deploy infrastructure in cloud provider trigger by commits in <u>IaC repository</u> 
- Create simple logging system to save log's of pod's(fluentd or etc)

## Intro and general steps of provision K8s cluster
This repository define for storage of code for provision K8s cluster to our cloud provider. There is folder helm in which you can find helm-chart for deploy our cluster. If you want read more detailed about it, please read helm/web-app/readme.md

<u>CI/CD pipeline is work in next steps</u>:

- If we have commit in <u>dev/main</u> branch without external trigger, we deploy to dev/prod environment from devops group. In this condition we  only can change the K8s files version, the version of app will be the same as before commit.

- If we have external trigger with external <u>tag="\*-dev"</u>, it means we will deploy to dev environment from developers group and we change the version of app to version with this tag. The same will be to prod environment, if external <u>tag="\*-prod"</u>.

Basic steps:
- Try connect to cluster 
- Linting the helm-chart
- Deploy helm-chart to environment
- If deploy failed, rollback to previous version
- List helm release at the end of pipeline to see the result of deploying

## Attention
The version of release chart  must be in format: [0-9].[0-9].[0-9]
The version of deploying app to dev environment must be in format: [a-z].[0-9].[0-9].[0-9]-dev
The version of deploying app to prod environment must be in format: [a-z].[0-9].[0-9].[0-9]-prod

<p align="center">
  <img src="./diagram_2.png" />
</p>

At this diagramm we can see architecture of our infrustructure. There are "App-pod's" - as a frontend-backend app on flask, which can provide static file's, process api-requests and send sql-request to database instances thorough "ProxySQL-pod's". "ProxySQL-pod's" is use for forwarding sql-requests to databases follow defined write/read rules. Database instances are existing RDS-db. 