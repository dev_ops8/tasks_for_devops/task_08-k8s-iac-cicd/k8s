############################# Deploying to dev environment ###############################

variables:
  nm_1: mysql-nm
  nm_2: app-nm
  region: eu-central-1
  cluster_dev: K8s-Web-app-dev        # depends from cluster name in tv file's
  namespace: helm
  release_dev: web-app-dev            # name of release in environment

stages:
  - "connect-to-cluster"
  - "testing"
  - "deploy"
  - "rollback"

# Common part of conncet-to-cluster job
.conncet-to-cluster:
  stage: "connect-to-cluster"
  tags:
    - runner

# Common part of testing job
.testing:
  stage: "testing"
  script:
    - helm lint ./helm/web-app
  tags:
    - runner

# Common part of deploy jobs
.deploy_job:
  stage: "deploy"
  before_script:
    - sed s#secret_name_docker#${secret_name_docker}#g -i ./helm/web-app/values.yaml
    - sed s#secret_name#${secret_name}#g -i ./helm/web-app/values.yaml
    - if ! kubectl get namespace ${nm_1}; then kubectl create namespace ${nm_1}; fi     
    - if ! kubectl get namespace ${nm_2}; then kubectl create namespace ${nm_2}; fi  
    - if ! kubectl get secrets -n ${nm_2} ${secret_name_docker} > /dev/null 2>&1; then kubectl create secret docker-registry ${secret_name_docker} -n ${nm_2} --docker-server=${DUMMY_SERVER} --docker-username=${DUMMY_USERNAME} --docker-password=${DUMMY_DOCKER_PASSWORD} --docker-email=dummy@mail.com > /dev/null 2>&1; fi
    - if ! kubectl get secrets -n ${nm_2} ${secret_name} > /dev/null 2>&1; then kubectl create secret generic ${secret_name} -n ${nm_2} --from-literal=${VARIABLE_1}=${VALUE_1} --from-literal=${VARIABLE_2}=${VALUE_2} > /dev/null 2>&1; fi
    - helm upgrade --install ${release_dev} ./helm/web-app -n ${namespace} --create-namespace --dry-run > /dev/null 
  after_script:
    - helm list -n ${namespace}
  tags:
    - runner

# If any deploy job fail we rollback to previous version
.rollback:
  stage: "rollback"
  when: on_failure
  after_script:
    - helm list -n ${namespace}
  tags:
    - runner

# Try to connect to EKS cluster on dev environment
connect-to-cluster-dev:
  extends:
    - .conncet-to-cluster
  script:
    - aws eks update-kubeconfig --region ${region} --name ${cluster_dev}

# Test the helm-chart code for dev environment
testing-dev:
  extends:
    - .testing

# Deploy to K8s cluster from devops saving previous version of app
deploy-from-devops-dev:
  rules:
    - if: '$TAG_D == "devops"'
      when: always
  extends:
    - .deploy_job
  script:
    - TAG=$(helm list -n ${namespace} | awk 'FNR == 2 {print $10}')
    - sed s#'[a-z]\.\+[0-9]\+\.\+[0-9]\+\.\+[0-9]\+.*'#${TAG}#g -i ./helm/web-app/Chart.yaml
    - sed s#docker_image#$TAG#g -i ./helm/web-app/values.yaml
    - helm upgrade --install ${release_dev} ./helm/web-app -n ${namespace} --create-namespace --wait --debug > /dev/null

# Deploy to K8s cluster via external trigger from developer's team to dev environment
deploy-from-developers-dev:
  rules:
    - if: '$TAG =~ /.*\-dev$/'
      when: always
  extends:
    - .deploy_job 
  script:
    - sed s#'[a-z]\.\+[0-9]\+\.\+[0-9]\+\.\+[0-9]\+.*'#${TAG}#g -i ./helm/web-app/Chart.yaml
    - sed s#docker_image#${TAG}#g -i ./helm/web-app/values.yaml
    - helm upgrade --install ${release_dev} ./helm/web-app -n ${namespace} --create-namespace --wait --debug > /dev/null

# If deploy-from-devops fail we rollback to previous version 
rollback-devops-dev:
  extends:
    - .rollback
  needs:
    - job: "deploy-from-devops-dev"
      optional: true
  script:
    - helm rollback -n ${namespace} ${release_dev}

# If deploy-from-developers fail we rollback to previous version 
rollback-developers-dev:
  extends:
    - .rollback
  needs:
    - job: "deploy-from-developers-dev"
      optional: true
  script:
    - helm rollback -n ${namespace} ${release_dev}