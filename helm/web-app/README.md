## Steps of provision web-app by Helm

First of all we can create necessary namespaces and secrets by kubectl.
#### Create necessary namespaces
- kubectl create namespace namespace_1 
- kubectl create namespace manespace_2

#### Create secrets for login to private registry
- kubectl create secret docker-registry myregistrykey -n namespace_2 --docker-server=DUMMY_SERVER \
        --docker-username=DUMMY_USERNAME --docker-password=DUMMY_DOCKER_PASSWORD \
        --docker-email=DUMMY_DOCKER_EMAIL

#### Create secrets for env variables which wil be pass to web-app containers
- kubectl create secret generic app -n namespace_2 \                                                                
        --from-literal=VARIABLE_NAME=VALUE \
        --from-literal=VARIABLE_NAME=VALUE

Provisioning web-app has dependencies. Before installation web-app chart, the proxysql-chart will be install at first.

### Steps of provision

#### Description
Proxysql is used to proxy via some rule's to different endpoint's of mysql database's. In mostly cases it use "read-only"/"write" rule's. This tool is necessary, when you use cluster "master-replica" of database's for more availability and performance. You can provide this config to instance with installed db(bare-metal) or for K8s pod's db. 
Web-app is dockerized app based on flask and guincorn with necessary env variables that must be pass to the container's. This app will be connected to db instances through proxysql pod's.
 
#### proxysql-chart
Please, carefully study the "configmap.yaml" in charts/proxysql/templates/configmap.yaml, deployment.yaml and values.yaml in root templates. In those file's you can find configuration of proxysql as config-map for K8s. 
You can add or change some expression on  your choice. Also you can hide some secrets in configmap.yaml (as a password, user and etc), and replace this sensitive value on variables. This variables can replace on necessary data use secret's vaults of you CI/CD tool or external secret operator's like Vault, AWS param store, AWS Secret manager and etc.
Pay attention on param "mysql_replication_hostgroups" this param stay by default. Proxysql can define by itself which mysql_server has write permission or don't. This is because we define "monitor_user" and "monitor_password" above, so proxysql does test connection to instances in "mysql_servers". If param "read_only" in my.cnf file on this instances set to 1 value, we can't write to this instance and can only read, if "read_only" set to 0 value, we can write/read. So if you want to define writer/reader hostgroup please define the param "read_only" in my.cnf on your database instances.
All extra params or any info you can find on official proxysql webpages.

#### web-app-chart
Please, carefully study all .yaml in root templates.
The example of basic env variables for web-app pod's:
  FLASK_APP: app
  FLASK_ENV: production
  SQLALCHEMY_ECHO: "True"
  SECRET_KEY: lkasjdf09ajsdkfljalsiorj12n3490re9485309irefvn,u90818734902139489230 
  DATABASE_URL: mysql://db_user:db_password@server_adress:port/db_name

#### Results
After finish deploy we have 2 pod's of proxysql with RollingUpdate strategy, 2 pod's of web-app with RollingUpdate strategy control by horizontal-pod-scaling, Headless type service proxysql for forwarding to proxysql pod's and LoadBalancer service in our cloud provider for web-app in defined namespace. So, now we can receive requests from WWW to web-app pod's and can forward to db instances via proxysql if necessary.